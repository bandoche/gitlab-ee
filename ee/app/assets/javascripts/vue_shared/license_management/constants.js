// eslint-disable-next-line import/prefer-default-export
export const LICENSE_APPROVAL_STATUS = {
  APPROVED: 'approved',
  BLACKLISTED: 'blacklisted',
};
